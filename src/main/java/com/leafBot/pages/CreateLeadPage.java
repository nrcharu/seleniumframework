package com.leafBot.pages;

import org.openqa.selenium.WebElement;

import com.leafBot.testng.api.base.Annotations;

public class CreateLeadPage extends Annotations {
	
	public CreateLeadPage typeCompanyName(String cName) {
		/*driver.findElementById("createLeadForm_companyName")
		.sendKeys(cName);*/
		WebElement ele = locateElement("id", "createLeadForm_companyName");
		clearAndType(ele, cName);
		return this;
	}
	
	public CreateLeadPage typeFirstName(String fName) {
		/*driver.findElementById("createLeadForm_firstName")
		.sendKeys(fName);*/
		WebElement ele = locateElement("id", "createLeadForm_firstName");
		clearAndType(ele, fName);
		return this;
	}
	
	public CreateLeadPage typeLastName(String lName) {
		/*driver.findElementById("createLeadForm_lastName")
		.sendKeys(lName);*/
		WebElement ele = locateElement("id", "createLeadForm_lastName");
		clearAndType(ele, lName);
		return this;
	}
	
	public ViewLeadPage clickCreateLeadButton() {
		/*driver.findElementByClassName("smallSubmit")
		.click();*/
		WebElement ele = locateElement("class", "smallSubmit");
		click(ele);
		return new ViewLeadPage();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
