package com.leafBot.pages;

import org.openqa.selenium.WebElement;

import com.leafBot.testng.api.base.Annotations;

public class MyLeadsPage extends Annotations {
	
	public CreateLeadPage clickCreateLead() {
		/*driver.findElementByLinkText("Create Lead")
		.click();
		return new CreateLeadPage();*/
		WebElement ele = locateElement("link", "Create Lead");
		click(ele);
		return new CreateLeadPage();
	}
	
	/*public FindLeadsPage clickFindLead() {
		driver.findElementByLinkText("Find Leads").click();
		return new FindLeadsPage();
	}*/

}
