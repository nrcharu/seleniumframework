package com.leafBot.pages;

import org.openqa.selenium.WebElement;

import com.leafBot.testng.api.base.Annotations;

public class ViewLeadPage extends Annotations {
	
	public ViewLeadPage verifyFirstName(String fName) {
		/*String text = driver.findElementById("viewLead_firstName_sp")
		.getText();*/
		WebElement ele = locateElement("id", "viewLead_firstName_sp");
		verifyExactText(ele,fName);
		/*if(text.equals("Sethu")) {
			System.out.println("First name matches with input data");
		}else {
			System.err.println("First name not matches with input data");
		}*/
		return this;
	}

	public MyLeadsPage clickDelete() {
		driver.findElementByLinkText("Delete").click();
		return new MyLeadsPage();
	}
	
	
}
